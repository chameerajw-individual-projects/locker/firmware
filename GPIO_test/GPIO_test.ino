/*uint8_t GPIO_Pin = D2;
  volatile int count = 0;

  void setup() {
  Serial.begin(9600);
  //attachInterrupt(digitalPinToInterrupt(GPIO_Pin), IntCallback, RISING);
  }

  void loop() {
  Serial.println(count);
  delay(500);
  }

  void IntCallback(){
  count++;
  }
*/

//uint8_t GPIO_Pin = D2;

const int count = 10;

uint8_t GPIO_Pin_array[count] = {D0, D1, D2, D3, D4, D5, D6, D7, D8,11};

void setup() {
  Serial.begin(9600);
  //attachInterrupt(digitalPinToInterrupt(GPIO_Pin), IntCallback, RISING);
  //pinMode(GPIO_Pin, OUTPUT);
  for (int i = 0; i < count; i++)
  {
    pinMode(GPIO_Pin_array[i], OUTPUT);
  }
}

void loop() {

  for (int i = 0; i < count; i++)
  {
    digitalWrite(GPIO_Pin_array[i], HIGH);
  }
  delay(1000);
  for (int i = 0; i < count; i++)
  {
    digitalWrite(GPIO_Pin_array[i], LOW);
  }
  delay(1000);

}

void IntCallback() {
  Serial.print("Stamp(ms): ");
  Serial.println(millis());
}
